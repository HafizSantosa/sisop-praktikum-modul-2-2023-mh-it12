#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

void dwnlod() {
    char newdir_name[100];
    char newfile_name[100];
    char tes[100];
    char dirPath[200];

    struct tm* dirname;
    time_t teOne;
    teOne = time(NULL);
    dirname = localtime(&teOne);

    strftime(newdir_name, sizeof(newdir_name), "[%Y-%m-%d_%H:%M:%S]", dirname);
    mkdir(newdir_name, 0700);


    for(int i=0; i<7; i++) {
        struct tm* filename;
        time_t teTwo;
        teTwo = time(NULL);
        filename = localtime(&teTwo);
        strftime(newfile_name, sizeof(newfile_name), "[%Y-%M-%d_%H:%M:%S]", filename);
        sprintf(dirPath, "%s/%s", newdir_name, newfile_name);

        pid_t pid = fork();

        if (pid == 0) {
            int tim = teTwo%1000 + 50;
            sprintf(tes, "https://source.unsplash.com/%dx%d", tim, tim);
            execl("/usr/bin/wget", "wget", tes, "-O", dirPath, NULL);
            exit(1);
        } else {
            wait(NULL);
        }
        sleep(5);
    }

    pid_t pid_zip = fork();
    if (pid_zip == 0) {
        char zipfile[110];

        sprintf(zipfile, "%s.zip", newdir_name);
        execl("/usr/bin/zip", "zip", "-r", zipfile, newdir_name, NULL);
        exit(1);
    } else {
        wait(NULL);
    }

    pid_t pid_del = fork();
    if (pid_del == 0) {
        execl("/bin/rm", "rm", "-rf", newdir_name, NULL);
        exit(1);
    } else {
        wait(NULL);
    }
}

void MODE_A() {
    printf("Hello A!\n");
}

void MODE_B() {
    printf("Hello B!\n");
}

int main(int argc, char *argv[]) {
    if (strcmp(argv[1], "-a") == 0) {
        MODE_A();
    } else if (strcmp(argv[1], "-b") == 0) {
        MODE_B();
    } else {
        printf("Invalid Prompt!");
        return 0;
    }

    pid_t pidDae = fork();

    if (pidDae < 0) {
        exit(EXIT_FAILURE);
    }
    if (pidDae > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    pid_t sid = setsid();
    if(sid < 0) {
        exit(EXIT_FAILURE);
    }

    //close(STDIN_FILENO);
    //close(STDOUT_FILENO);
    //close(STDERR_FILENO);

    pid_t theRoot;
    theRoot = fork();
    while(1) {
        if(theRoot == 0) {
            dwnlod();
        }
        sleep(30);
    }

    return 0;
}