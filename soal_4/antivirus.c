#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <wait.h>
#include <time.h> 

enum SecurityLevel {
    LOW,
    MEDIUM,
    HARD
};

void rotate13(char *str) {
    for (int i = 0; str[i]; i++) {
        char c = str[i];
        if (c >= 'a' && c <= 'z') {
            str[i] = 'a' + ((c - 'a' + 13) % 26);
        } else if (c >= 'A' && c <= 'Z') {
            str[i] = 'A' + ((c - 'A' + 13) % 26);
        }
    }
}


void decryptExtensions() {
    FILE *extensionsFile = fopen("extensions.csv", "r");
    if (extensionsFile == NULL) {
        perror("Error opening extensions.csv");
        exit(EXIT_FAILURE);
    }

    char extension[256];
    while (fgets(extension, sizeof(extension), extensionsFile)) {
        // Dekripsi ekstensi menggunakan ROT13
        rotate13(extension);

      
    }
    fclose(extensionsFile);
}

void handleSignal(int signo) {
    if (signo == SIGUSR1) {
        // Ganti ke level "low"
    } else if (signo == SIGUSR2) {
        // Ganti ke level "medium"
    } else if (signo == SIGRTMIN) {
        // Ganti ke level "hard"
    }
}

void runAsDaemon(enum SecurityLevel securityLevel) {
    pid_t pid, sid;

    // Fork menjadi background process
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    // Tutup file descriptor yang tidak diperlukan
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // Mengatur penanganan sinyal
    signal(SIGUSR1, handleSignal);
    signal(SIGUSR2, handleSignal);
    signal(SIGRTMIN, handleSignal);

    // Dekripsi ekstensi dari "extensions.csv"
    decryptExtensions();

    // Monitoring folder sisop_infected menggunakan inotify
    int inotifyFd = inotify_init();
    if (inotifyFd == -1) {
        perror("inotify_init");
        exit(EXIT_FAILURE);
    }

    int wd = inotify_add_watch(inotifyFd, "sisop_infected", IN_CLOSE_WRITE);
    if (wd == -1) {
        perror("inotify_add_watch");
        exit(EXIT_FAILURE);
    }

    // Loop utama
    while (1) {

        if (securityLevel == LOW) {
        } else if (securityLevel == MEDIUM) {
        } else if (securityLevel == HARD) {
        }
    }
}

int main(int argc, char *argv[]) {
    // Parse argumen untuk level keamanan
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <security_level>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    enum SecurityLevel securityLevel;
    if (strcmp(argv[1], "low") == 0) {
        securityLevel = LOW;
    } else if (strcmp(argv[1], "medium") == 0) {
        securityLevel = MEDIUM;
    } else if (strcmp(argv[1], "hard") == 0) {
        securityLevel = HARD;
    } else {
        fprintf(stderr, "Invalid security level\n");
        exit(EXIT_FAILURE);
    }

    runAsDaemon(securityLevel);

    return 0;
}
