#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>

void createFolder(const char *folderName)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"mkdir", folderName, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

void downloadFile(const char *url, const char *outputFileName)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", url, "-O", outputFileName, NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

void unzipFile(const char *zipFileName, const char *outputFolder)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"unzip", zipFileName, "-d", outputFolder, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

void deleteFilesWithoutCavaliersKeyword(const char *folderPath)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "Cavaliers") == NULL)
        {
            char filepath[256];
            snprintf(filepath, sizeof(filepath), "%s/%s", folderPath, entry->d_name);

            pid_t child_pid = fork();
            if (child_pid == 0)
            {
                char *argv[] = {"rm", filepath, NULL};
                execv("/bin/rm", argv);
                exit(0);
            }
            else
            {
                wait(NULL);
                printf("Deleted file: %s\n", filepath);
            }
        }
    }

    closedir(dir);
}

void countPlayersByPosition(const char *folderPath, int *counts)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *filename = strdup(entry->d_name);

            if (strstr(filename, "-PG-") != NULL)
            {
                counts[0]++;
            }
            else if (strstr(filename, "-SG-") != NULL)
            {
                counts[1]++;
            }
            else if (strstr(filename, "-SF-") != NULL)
            {
                counts[2]++;
            }
            else if (strstr(filename, "-PF-") != NULL)
            {
                counts[3]++;
            }
            else if (strstr(filename, "-C-") != NULL)
            {
                counts[4]++;
            }

            free(filename);
        }
    }

    closedir(dir);
}

void movePlayersToPositionFolders(const char *folderPath)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *filename = strdup(entry->d_name);

            if (strstr(filename, "-PG-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/PG", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/PG/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-SG-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/SG", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/SG/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-SF-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/SF", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/SF/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-PF-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/PF", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/PF/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-C-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/C", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/C/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }

            free(filename);
        }
    }

    closedir(dir);
}

void copyLeBronAndKyrieToClutchFolder()
{
    // Buat folder "clutch"
    createFolder("clutch");

    // Copy file LeBron James dari folder pemain masing-masing
    char *lebronPlayers[] = {"PG", "SG", "SF", "PF", "C"};
    char destLeBron[256];

    for (int i = 0; i < 5; i++)
    {
        char srcLeBron[256];
        snprintf(srcLeBron, sizeof(srcLeBron), "players/%s", lebronPlayers[i]);
        DIR *dir = opendir(srcLeBron);
        struct dirent *entry;

        if (dir)
        {
            while ((entry = readdir(dir)) != NULL)
            {
                if (entry->d_type == DT_REG && strstr(entry->d_name, "LeBron-James") != NULL)
                {
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s/%s", srcLeBron, entry->d_name);

                    snprintf(destLeBron, sizeof(destLeBron), "clutch/%s", entry->d_name);

                    pid_t copyLeBron;
                    copyLeBron = fork();
                    if (copyLeBron == 0)
                    {
                        char *argv[] = {"cp", filename, destLeBron, NULL};
                        execv("/bin/cp", argv);
                    }
                    wait(NULL);
                }
            }
            closedir(dir);
        }
    }

    // Copy file Kyrie Irving dari folder pemain masing-masing
    char *kyriePlayers[] = {"PG", "SG", "SF", "PF", "C"};
    char destKyrie[256];

    for (int i = 0; i < 5; i++)
    {
        char srcKyrie[256];
        snprintf(srcKyrie, sizeof(srcKyrie), "players/%s", kyriePlayers[i]);
        DIR *dir = opendir(srcKyrie);
        struct dirent *entry;

        if (dir)
        {
            while ((entry = readdir(dir)) != NULL)
            {
                if (entry->d_type == DT_REG && strstr(entry->d_name, "Kyrie-Irving") != NULL)
                {
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s/%s", srcKyrie, entry->d_name);

                    snprintf(destKyrie, sizeof(destKyrie), "clutch/%s", entry->d_name);

                    pid_t copyKyrie;
                    copyKyrie = fork();
                    if (copyKyrie == 0)
                    {
                        char *argv[] = {"cp", filename, destKyrie, NULL};
                        execv("/bin/cp", argv);
                    }
                    wait(NULL);
                }
            }
            closedir(dir);
        }
    }

    printf("Copied LeBron James and Kyrie Irving to clutch folder\n");
}

int main() {
    // Langkah 1: Membuat folder "players"
    createFolder("players");

    // Langkah 2: Mengunduh dan mengekstrak database pemain
    downloadFile("https://docs.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "players.zip");
    unzipFile("players.zip", "players");

    // Langkah 3: Hapus file yang bukan pemain Cavaliers
    deleteFilesWithoutCavaliersKeyword("players");

    // Langkah 4: Hitung jumlah pemain dalam setiap posisi
    int playerCounts[5] = {0};
    countPlayersByPosition("players", playerCounts);

    // Simpan hasil perhitungan ke dalam file Formasi.txt
    FILE *outputFile = fopen("Formasi.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file");
        exit(1);
    }

    fprintf(outputFile, "PG: %d\n", playerCounts[0]);
    fprintf(outputFile, "SG: %d\n", playerCounts[1]);
    fprintf(outputFile, "SF: %d\n", playerCounts[2]);
    fprintf(outputFile, "PF: %d\n", playerCounts[3]);
    fprintf(outputFile, "C: %d\n", playerCounts[4]);

    fclose(outputFile);

    // Langkah 5: Membuat folder-folder posisi pemain dan memindahkan pemain ke dalamnya
    movePlayersToPositionFolders("players");

    // Tambahan: Copy LeBron James dan Kyrie Irving ke folder "clutch"
    copyLeBronAndKyrieToClutchFolder();

    return 0;
}

//Tidak sesuai urutan poin soal tapi hasil sesuai yang diharapkan
