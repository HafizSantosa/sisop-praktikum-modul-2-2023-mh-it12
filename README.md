# sisop-praktikum-modul-2-2023-MH-IT12

# Laporan Resmi Praktikum Sistem Operasi Modul 2

Kelompok IT12:
  + Muhammad Afif (5027221032)
  + Hafiz Akmaldi Santosa (5027221061)
  + Nur Azka Rahadiansyah (5027221064)

# Pengantar
Laporan resmi ini dibuat terkait dengan praktikum modul 1 sistem operasi yang telah dilaksanakan pada tanggal xx hingga tanggal xx. Praktikum modul 1 terdiri dari 4 soal dengan tema C yang dikerjakan oleh kelompok praktikan yang terdiri dari 3 orang selama waktu tertentu.

Kelompok IT12 melakukan pengerjaan modul 2 ini dengan pembagian sebagai berikut:
  + Soal 2 dikerjakan oleh Hafiz Akmaldi Santosa
  + Soal 3 dikerjakan oleh Muhammad Afif
  + Soal 4 dikerjakan oleh Nur Azka R.
Sehingga dengan demikian, semua anggota kelompok memiliki peran yang sama besar dalam pengerjaan modul 2 ini.

Kelompok IT12 juga telah menyelesaikan tugas praktikum modul 2 yang telah diberikan dan telah melakukan demonstrasi kepada Asisten lab penguji mas aloy. Dari hasil praktikum yang telah dilakukan sebelumnya, maka diperoleh hasil sebagaimana yang dituliskan pada setiap bab di bawah ini.

## Soal 2
(Hafiz)
Soal 2 bermaksud untuk membuat program C yang dapat melakukan sortir file yang diberikan.

### Isi soal
QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:
- Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.
- Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.
- Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).
- Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
PG: {jumlah pemain}
SG: {jumlah pemain}
SF: {jumlah pemain}
PF: {jumlah pemain}
C: {jumlah pemain}
- Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan.
- Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.

### Catatan:
Format nama file yang akan diunduh dalam zip berupa [tim]-[posisi]-[nama].png
Tidak boleh menggunakan system(), Gunakan exec() dan fork().
Directory “.” dan “..” tidak termasuk yang akan dihapus.
2 poin soal terakhir dilakukan setelah proses kategorisasi selesai.

### Penyelesaian

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>

void createFolder(const char *folderName)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"mkdir", folderName, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

void downloadFile(const char *url, const char *outputFileName)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", url, "-O", outputFileName, NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

void unzipFile(const char *zipFileName, const char *outputFolder)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"unzip", zipFileName, "-d", outputFolder, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

void deleteFilesWithoutCavaliersKeyword(const char *folderPath)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "Cavaliers") == NULL)
        {
            char filepath[256];
            snprintf(filepath, sizeof(filepath), "%s/%s", folderPath, entry->d_name);

            pid_t child_pid = fork();
            if (child_pid == 0)
            {
                char *argv[] = {"rm", filepath, NULL};
                execv("/bin/rm", argv);
                exit(0);
            }
            else
            {
                wait(NULL);
                printf("Deleted file: %s\n", filepath);
            }
        }
    }

    closedir(dir);
}

void countPlayersByPosition(const char *folderPath, int *counts)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *filename = strdup(entry->d_name);

            if (strstr(filename, "-PG-") != NULL)
            {
                counts[0]++;
            }
            else if (strstr(filename, "-SG-") != NULL)
            {
                counts[1]++;
            }
            else if (strstr(filename, "-SF-") != NULL)
            {
                counts[2]++;
            }
            else if (strstr(filename, "-PF-") != NULL)
            {
                counts[3]++;
            }
            else if (strstr(filename, "-C-") != NULL)
            {
                counts[4]++;
            }

            free(filename);
        }
    }

    closedir(dir);
}

void movePlayersToPositionFolders(const char *folderPath)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *filename = strdup(entry->d_name);

            if (strstr(filename, "-PG-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/PG", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/PG/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-SG-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/SG", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/SG/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-SF-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/SF", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/SF/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-PF-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/PF", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/PF/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }
            else if (strstr(filename, "-C-") != NULL)
            {
                char newFolderPath[256];
                snprintf(newFolderPath, sizeof(newFolderPath), "%s/C", folderPath);
                createFolder(newFolderPath);
                char newFilePath[256];
                snprintf(newFilePath, sizeof(newFilePath), "%s/C/%s", folderPath, entry->d_name);
                char oldFilePath[256];
                snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", folderPath, entry->d_name);
                rename(oldFilePath, newFilePath);
            }

            free(filename);
        }
    }

    closedir(dir);
}

void copyLeBronAndKyrieToClutchFolder()
{
    // Buat folder "clutch"
    createFolder("clutch");

    // Copy file LeBron James dari folder pemain masing-masing
    char *lebronPlayers[] = {"PG", "SG", "SF", "PF", "C"};
    char destLeBron[256];

    for (int i = 0; i < 5; i++)
    {
        char srcLeBron[256];
        snprintf(srcLeBron, sizeof(srcLeBron), "players/%s", lebronPlayers[i]);
        DIR *dir = opendir(srcLeBron);
        struct dirent *entry;

        if (dir)
        {
            while ((entry = readdir(dir)) != NULL)
            {
                if (entry->d_type == DT_REG && strstr(entry->d_name, "LeBron-James") != NULL)
                {
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s/%s", srcLeBron, entry->d_name);

                    snprintf(destLeBron, sizeof(destLeBron), "clutch/%s", entry->d_name);

                    pid_t copyLeBron;
                    copyLeBron = fork();
                    if (copyLeBron == 0)
                    {
                        char *argv[] = {"cp", filename, destLeBron, NULL};
                        execv("/bin/cp", argv);
                    }
                    wait(NULL);
                }
            }
            closedir(dir);
        }
    }

    // Copy file Kyrie Irving dari folder pemain masing-masing
    char *kyriePlayers[] = {"PG", "SG", "SF", "PF", "C"};
    char destKyrie[256];

    for (int i = 0; i < 5; i++)
    {
        char srcKyrie[256];
        snprintf(srcKyrie, sizeof(srcKyrie), "players/%s", kyriePlayers[i]);
        DIR *dir = opendir(srcKyrie);
        struct dirent *entry;

        if (dir)
        {
            while ((entry = readdir(dir)) != NULL)
            {
                if (entry->d_type == DT_REG && strstr(entry->d_name, "Kyrie-Irving") != NULL)
                {
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s/%s", srcKyrie, entry->d_name);

                    snprintf(destKyrie, sizeof(destKyrie), "clutch/%s", entry->d_name);

                    pid_t copyKyrie;
                    copyKyrie = fork();
                    if (copyKyrie == 0)
                    {
                        char *argv[] = {"cp", filename, destKyrie, NULL};
                        execv("/bin/cp", argv);
                    }
                    wait(NULL);
                }
            }
            closedir(dir);
        }
    }

    printf("Copied LeBron James and Kyrie Irving to clutch folder\n");
}

int main() {
    createFolder("players");

    downloadFile("https://docs.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "players.zip");
    
    unzipFile("players.zip", "players");

    deleteFilesWithoutCavaliersKeyword("players");

    int playerCounts[5] = {0};
    countPlayersByPosition("players", playerCounts);

    FILE *outputFile = fopen("Formasi.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file");
        exit(1);
    }

    fprintf(outputFile, "PG: %d\n", playerCounts[0]);
    fprintf(outputFile, "SG: %d\n", playerCounts[1]);
    fprintf(outputFile, "SF: %d\n", playerCounts[2]);
    fprintf(outputFile, "PF: %d\n", playerCounts[3]);
    fprintf(outputFile, "C: %d\n", playerCounts[4]);

    fclose(outputFile);

    movePlayersToPositionFolders("players");

    copyLeBronAndKyrieToClutchFolder();

    return 0;
}
```

### Penjelasan

- Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”. Berikut adalah fungsi untuk membuat folder baru:
```c
void createFolder(const char *folderName)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"mkdir", folderName, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

int main() {

	createFolder("players");

    return 0;
}
```
- Program akan mengunduh file yang berisikan database para pemain bola.
```c
void downloadFile(const char *url, const char *outputFileName)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", url, "-O", outputFileName, NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}
int main() {

    downloadFile("https://docs.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "players.zip");

    return 0;
}
```

- Kemudian diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat.

```c
void unzipFile(const char *zipFileName, const char *outputFolder)
{
    pid_t child_pid = fork();
    if (child_pid == 0)
    {
        char *argv[] = {"unzip", zipFileName, "-d", outputFolder, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    else
    {
        wait(NULL);
    }
}

int main() {

	unzipFile("players.zip", "players");

    return 0;
}
```

- Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.

```c
void deleteFilesWithoutCavaliersKeyword(const char *folderPath)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG && strstr(entry->d_name, "Cavaliers") == NULL)
        {
            char filepath[256];
            snprintf(filepath, sizeof(filepath), "%s/%s", folderPath, entry->d_name);

            pid_t child_pid = fork();
            if (child_pid == 0)
            {
                char *argv[] = {"rm", filepath, NULL};
                execv("/bin/rm", argv);
                exit(0);
            }
            else
            {
                wait(NULL);
                printf("Deleted file: %s\n", filepath);
            }
        }
    }

    closedir(dir);
}

int main() {

	deleteFilesWithoutCavaliersKeyword("players");

    return 0;
}
```

- kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
PG: {jumlah pemain}
SG: {jumlah pemain}
SF: {jumlah pemain}
PF: {jumlah pemain}
C: {jumlah pemain}

```c
void countPlayersByPosition(const char *folderPath, int *counts)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *filename = strdup(entry->d_name);

            if (strstr(filename, "-PG-") != NULL)
            {
                counts[0]++;
            }
            else if (strstr(filename, "-SG-") != NULL)
            {
                counts[1]++;
            }
            else if (strstr(filename, "-SF-") != NULL)
            {
                counts[2]++;
            }
            else if (strstr(filename, "-PF-") != NULL)
            {
                counts[3]++;
            }
            else if (strstr(filename, "-C-") != NULL)
            {
                counts[4]++;
            }

            free(filename);
        }
    }

    closedir(dir);
}

int main() {

	int playerCounts[5] = {0};
    	countPlayersByPosition("players", playerCounts);
	FILE *outputFile = fopen("Formasi.txt", "w");
    	if (outputFile == NULL) {
        	perror("Error opening output file");
        	exit(1);
    	}

	fprintf(outputFile, "PG: %d\n", playerCounts[0]);
    	fprintf(outputFile, "SG: %d\n", playerCounts[1]);
    	fprintf(outputFile, "SF: %d\n", playerCounts[2]);
    	fprintf(outputFile, "PF: %d\n", playerCounts[3]);
    	fprintf(outputFile, "C: %d\n", playerCounts[4]);

    	fclose(outputFile);

    return 0;
}

```

- Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).

```c
void countPlayersByPosition(const char *folderPath, int *counts)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(folderPath);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *filename = strdup(entry->d_name);

            if (strstr(filename, "-PG-") != NULL)
            {
                counts[0]++;
            }
            else if (strstr(filename, "-SG-") != NULL)
            {
                counts[1]++;
            }
            else if (strstr(filename, "-SF-") != NULL)
            {
                counts[2]++;
            }
            else if (strstr(filename, "-PF-") != NULL)
            {
                counts[3]++;
            }
            else if (strstr(filename, "-C-") != NULL)
            {
                counts[4]++;
            }

            free(filename);
        }
    }

    closedir(dir);
}

int main() {

	movePlayersToPositionFolders("players");

    return 0;
}

``` 

- Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan. Ia juga merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.

```c
void copyLeBronAndKyrieToClutchFolder()
{
    // Buat folder "clutch"
    createFolder("clutch");

    // Copy file LeBron James dari folder pemain masing-masing
    char *lebronPlayers[] = {"PG", "SG", "SF", "PF", "C"};
    char destLeBron[256];

    for (int i = 0; i < 5; i++)
    {
        char srcLeBron[256];
        snprintf(srcLeBron, sizeof(srcLeBron), "players/%s", lebronPlayers[i]);
        DIR *dir = opendir(srcLeBron);
        struct dirent *entry;

        if (dir)
        {
            while ((entry = readdir(dir)) != NULL)
            {
                if (entry->d_type == DT_REG && strstr(entry->d_name, "LeBron-James") != NULL)
                {
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s/%s", srcLeBron, entry->d_name);

                    snprintf(destLeBron, sizeof(destLeBron), "clutch/%s", entry->d_name);

                    pid_t copyLeBron;
                    copyLeBron = fork();
                    if (copyLeBron == 0)
                    {
                        char *argv[] = {"cp", filename, destLeBron, NULL};
                        execv("/bin/cp", argv);
                    }
                    wait(NULL);
                }
            }
            closedir(dir);
        }
    }

    // Copy file Kyrie Irving dari folder pemain masing-masing
    char *kyriePlayers[] = {"PG", "SG", "SF", "PF", "C"};
    char destKyrie[256];

    for (int i = 0; i < 5; i++)
    {
        char srcKyrie[256];
        snprintf(srcKyrie, sizeof(srcKyrie), "players/%s", kyriePlayers[i]);
        DIR *dir = opendir(srcKyrie);
        struct dirent *entry;

        if (dir)
        {
            while ((entry = readdir(dir)) != NULL)
            {
                if (entry->d_type == DT_REG && strstr(entry->d_name, "Kyrie-Irving") != NULL)
                {
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s/%s", srcKyrie, entry->d_name);

                    snprintf(destKyrie, sizeof(destKyrie), "clutch/%s", entry->d_name);

                    pid_t copyKyrie;
                    copyKyrie = fork();
                    if (copyKyrie == 0)
                    {
                        char *argv[] = {"cp", filename, destKyrie, NULL};
                        execv("/bin/cp", argv);
                    }
                    wait(NULL);
                }
            }
            closedir(dir);
        }
    }

    printf("Copied LeBron James and Kyrie Irving to clutch folder\n");
}

int main() {

	movePlayersToPositionFolders("players");

    return 0;
}
```
- Sehingga setelah dijalankan akan menghasilkan output sebagai berikut:

```

.
├── Formasi.txt
├── cavs
├── cavs.c
├── clutch
│   ├── Cavaliers-PG-Kyrie-Irving.png
│   └── Cavaliers-SF-LeBron-James.png
├── players
│   ├── C
│   │   ├── Cavaliers-C-Sasha-Kaun.png
│   │   ├── Cavaliers-C-Timofey-Mozgov.png
│   │   └── Cavaliers-C-Tristan-Thompson.png
│   ├── PF
│   │   └── Cavaliers-PF-Kevin-Love.png
│   ├── PG
│   │   ├── Cavaliers-PG-Jordan-McRae.png
│   │   ├── Cavaliers-PG-Kyrie-Irving.png
│   │   ├── Cavaliers-PG-Matthew-Dellavedova.png
│   │   └── Cavaliers-PG-Mo-Williams.png
│   ├── SF
│   │   ├── Cavaliers-SF-Dahntay-Jones.png
│   │   ├── Cavaliers-SF-James-Jones.png
│   │   ├── Cavaliers-SF-LeBron-James.png
│   │   └── Cavaliers-SF-Richard-Jefferson.png
│   └── SG
│       ├── Cavaliers-SG-Iman-Shumpert.png
│       ├── Cavaliers-SG-JR-Smith.png
│       └── Cavaliers-SG-Jared-Cunningham.png
└── players.zip

7 directories, 21 files
```

Serta isi dari Formasi.txt yaitu:
```
PG: 4
SG: 3
SF: 4
PF: 1
C: 3
```

## Soal 3
(Afif)
Soal ini bermaksud untuk membuat sebuah program C yang dapat berjalan secara daemon untuk mendownload 15 buah gambar sesuai ketentuan, meletakkannya pada folder lalu membentuk sebuah file zip dari foto-foto tersebut.

### Isi soal
Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

+ Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
+ Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
+ Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).
+ Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
+ Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).


### Catatan:
+ Tidak boleh menggunakan system()
+ Proses berjalan secara daemon
+ Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)


### Penyelesaian

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

void dwnlod() {
    char newdir_name[100];
    char newfile_name[100];
    char tes[100];
    char dirPath[200];

    struct tm* dirname;
    time_t teOne;
    teOne = time(NULL);
    dirname = localtime(&teOne);

    strftime(newdir_name, sizeof(newdir_name), "[%Y-%m-%d_%H:%M:%S]", dirname);
    mkdir(newdir_name, 0700);


    for(int i=0; i<15; i++) {
        struct tm* filename;
        time_t teTwo;
        teTwo = time(NULL);
        filename = localtime(&teTwo);
        strftime(newfile_name, sizeof(newfile_name), "[%Y-%M-%d_%H:%M:%S]", filename);
        sprintf(dirPath, "%s/%s", newdir_name, newfile_name);

        pid_t pid = fork();

        if (pid == 0) {
            int tim = teTwo%1000 + 50;
            sprintf(tes, "https://source.unsplash.com/%dx%d", tim, tim);
            execl("/usr/bin/wget", "wget", tes, "-O", dirPath, NULL);
            exit(1);
        } else {
            wait(NULL);
        }
        sleep(5);
    }

    pid_t pid_zip = fork();
    if (pid_zip == 0) {
        char zipfile[110];

        sprintf(zipfile, "%s.zip", newdir_name);
        execl("/usr/bin/zip", "zip", "-r", zipfile, newdir_name, NULL);
        exit(1);
    } else {
        wait(NULL);
    }

    pid_t pid_del = fork();
    if (pid_del == 0) {
        execl("/bin/rm", "rm", "-rf", newdir_name, NULL);
        exit(1);
    } else {
        wait(NULL);
    }
}

void MODE_A() {
    FILE* killer = fopen("killin.sh", "w");
    fprintf(killer, "#!/bin/bash\nkillall -9 lukisan;\nrm $0;\n");
    fclose(killer);

    pid_t chm = fork();
    if(chm == 0) {
        execlp("/usr/bin/chmod", "chmod", "+x", "killin.sh", NULL);
        exit(1);
    } else {
        wait(NULL);
    }
}

void MODE_B() {
   FILE* killer = fopen("killin.sh", "w");
    fprintf(killer, "#!/bin/bash\nkillparent(){\nkill${@:-1}\n}\nkill_parent $(pidof lukisan)\nrm $0;\n");
    fclose(killer);

    pid_t chm = fork();
    if(chm == 0) {
        execlp("/usr/bin/chmod", "chmod", "+x", "killin.sh", NULL);
        exit(1);
    } else {
        wait(NULL);
    }
}

void donlodLoop() {
    pid_t dwnloop = fork();
    if (dwnloop == 0) {
        dwnlod();
        exit(1);
    } else {
        sleep(30);
        donlodLoop();
    }
}

int main(int argc, char *argv[]) {
    if (strcmp(argv[1], "-a") == 0) {
        MODE_A();
    } else if (strcmp(argv[1], "-b") == 0) {
        MODE_B();
    } else {
        printf("Invalid Prompt!");
        return 0;
    }

    pid_t pidDae = fork();

    if (pidDae < 0) {
        exit(EXIT_FAILURE);
    }
    if (pidDae > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    pid_t sid = setsid();
    if(sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    donlodLoop();

    return 0;
}
```

### Penjelasan

```c
void dwnlod() {
    char newdir_name[100];
    char newfile_name[100];
    char tes[100];
    char dirPath[200];

    struct tm* dirname;
    time_t teOne;
    teOne = time(NULL);
    dirname = localtime(&teOne);

    strftime(newdir_name, sizeof(newdir_name), "[%Y-%m-%d_%H:%M:%S]", dirname);
    mkdir(newdir_name, 0700);
```
Bagian kode ini memiliki fungsi untuk membuat sebuah directory yang memiliki nama sesuai dengan waktu program tersebut dijalankan dalam format Epoch Unit. Namun bagian tersebut masih belum melakukan loop setiap 30 detik sekali. bagian loop akan diakukan nanti dalam pemanggilan fungsi ```donlodLoop()```.

```c
for(int i=0; i<15; i++) {
        struct tm* filename;
        time_t teTwo;
        teTwo = time(NULL);
        filename = localtime(&teTwo);
        strftime(newfile_name, sizeof(newfile_name), "[%Y-%M-%d_%H:%M:%S]", filename);
        sprintf(dirPath, "%s/%s", newdir_name, newfile_name);

        pid_t pid = fork();

        if (pid == 0) {
            int tim = teTwo%1000 + 50;
            sprintf(tes, "https://source.unsplash.com/%dx%d", tim, tim);
            execl("/usr/bin/wget", "wget", tes, "-O", dirPath, NULL);
            exit(1);
        } else {
            wait(NULL);
        }
        sleep(5);
    }
```
Bagian ini memiliki fungsi untuk mendownload file gambar dengan ukuran ```panjang = lebar = (t%1000)+50```. Gambar tersebut kemudian diberi nama sesuai dengan waktu program download tersebut dijalankan, karena diberi perintah ```sleep(5)``` maka diharapkan setiap nama file gambar memiliki jeda waktu 5 detik.

```c
pid_t pid_zip = fork();
    if (pid_zip == 0) {
        char zipfile[110];

        sprintf(zipfile, "%s.zip", newdir_name);
        execl("/usr/bin/zip", "zip", "-r", zipfile, newdir_name, NULL);
        exit(1);
    } else {
        wait(NULL);
    }
```
Setelah dilakukan download file gambar sebanyak 15 buah, dijalankan program di atas untuk men-zip file-file tersebut ke dalam sebuah file zip. File zip juga diberi nama yang sama dengan nama direktori tempat file-file gambar tersebut didownload

```c
pid_t pid_del = fork();
    if (pid_del == 0) {
        execl("/bin/rm", "rm", "-rf", newdir_name, NULL);
        exit(1);
    } else {
        wait(NULL);
}
```
Kemudian pada bagian tersebut, direktori tempat mendownload gambar tersebut dihapus tanpa jejak.

```c
void donlodLoop() {
    pid_t dwnloop = fork();
    if (dwnloop == 0) {
        dwnlod();
        exit(1);
    } else {
        sleep(30);
        donlodLoop();
    }
}
```
Fungsi yang dideklarasikan tersebut merupakan fungsi yang akan memanggil fungsi dwnlod yang merupakan fungsi utama sistem untuk membuat direktori dan mendownload gambar. Karena menggunakan fork, fungsi ini tidak akan menunggu fungsi yang dipanggil untuk menyelesaikan tugasnya. Fungsi ini hanya akan menunggu selama 30 detik dan kemudian akan memanggil dirinya sendiri secara rekursif.

```c
void MODE_A() {
    FILE* killer = fopen("killin.sh", "w");
    fprintf(killer, "#!/bin/bash\nkillall -9 lukisan;\nrm $0;\n");
    fclose(killer);

    pid_t chm = fork();
    if(chm == 0) {
        execlp("/usr/bin/chmod", "chmod", "+x", "killin.sh", NULL);
        exit(1);
    } else {
        wait(NULL);
    }
}
```
Pada MODE_A ini, fungsi akan dipanggil ketika argumen `./lukisan -a` dijalankan. Fungsi ini akan membuat sebuah bashfile bernama `killin.sh` yang dapat dijalankan untuk mematikan semua proses daemon `./lukisan` yang sedang berlangsung. Sementara pada MODE_B, cara mematikan programnya memiliki cara yang sedikit berbeda.

```c
void MODE_B() {
   FILE* killer = fopen("killin.sh", "w");
    fprintf(killer, "#!/bin/bash\nkillparent(){\nkill${@:-1}\n}\nkill_parent $(pidof lukisan)\nrm $0;\n");
    fclose(killer);

    pid_t chm = fork();
    if(chm == 0) {
        execlp("/usr/bin/chmod", "chmod", "+x", "killin.sh", NULL);
        exit(1);
    } else {
        wait(NULL);
    }
}
```
Pada MODE_B, fungsi akan dipanggil ketika argumen `./lukisan -b` dijalankan. Fungsi ini akan membuat sebuah bashfile dengan nama yang sama dengan MODE_A hanya saja cara file tersebut mematikan program daemon yang dijalankan adalah dengan mematikan PID parent dari program tersebut. Sehingga program child yang dijalankan dapat diselesaikan terlebih dahulu sebelum semua program benar-benar berhenti.

```c
int main(int argc, char *argv[]) {
    if (strcmp(argv[1], "-a") == 0) {
        MODE_A();
    } else if (strcmp(argv[1], "-b") == 0) {
        MODE_B();
    } else {
        printf("Invalid Prompt!");
        return 0;
    }

    pid_t pidDae = fork();

    if (pidDae < 0) {
        exit(EXIT_FAILURE);
    }
    if (pidDae > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    pid_t sid = setsid();
    if(sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    donlodLoop();

    return 0;
}
```
Pada fungsi main ini, fungsi akan membaca argumen terlebih dahulu. Sesuai dengan argumen yang dimasukkan, fungsi kemudian dapat memanggil MODE_A, MODE_B, ataupun memberikan output error. Bagian yang dimulai dari `pid_t pidDae = fork();` hingga `close(STDERR_FILENO);` merupakan format standar untuk menjadikan program yang dijalankan sebagai program daemon. Setelah semua persiapan dilakukan, program main kemudian akan memanggil program utama, yaitu program `dwnlod()` melalui pemanggilan program `donlodLoop();`.





## Soal 4
(Azka)
Soal 4 bermaksud untuk membuat program antivirus.c yang dapat mengecek file di dalam folder sisop_infected kemudian jika file terdeteksi low maka akan tercatat pada virus.log lalu jika terdeteksi medium maka akan dipindahkan kepada file quarantine dan tercatat di virus.log lalu yang terakhir hard yang akan menghapus file dari sisop_infected.

### Isi soal
Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system()
Daftar ekstensi file yang dianggap virus tersimpan dalam file extensions.csv.
Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:
```
[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}
```
Contoh:  
```
[sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine
```

*nama_user: adalah username dari user yang menambahkan file ter-infected

Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.
Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.

    Low: Hanya me-log file yg terdeteksi
    Medium: log dan memindahkan file yang terdeteksi
    Hard: log dan menghapus file yang terdeteksi

ex: ./antivirus -p low

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:

```c
kill -SIGUSR1 <pid_program>
```


Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.


### Catatan:
Format log harus sesuai dengan:
[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil} . dapat menjalankan kill -SIGUSR1 <pid_program>. 

### Penjelasan

```
void downloadExtensionsCSV() {
    char* command[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5", "-O", "extensions.csv", NULL};
    execv("/usr/bin/wget", command);

    perror("execv");
    exit(1);
}
```
Fungsi ini untuk mengunduh file extensions.csv menggunakan perintah wget. Fungsi ini membuat sebuah array argumen perintah untuk execv() dan menjalankan perintah wget untuk mengunduh file tersebut. Jika unduhan berhasil (status keluaran 0), program nantinya melanjutkan untuk mendekripsi dan memeriksa file yang terinfeksi. Jika unduhan gagal, program akan mencetak pesan kesalahan.

```
void decryptExtensionsCSV() {
    FILE *file = fopen("extensions.csv", "r+");
    if (file == NULL) {
        perror("File open error");
        exit(1);
    }

    int row = 0;
    char line[512];

    while (fgets(line, sizeof(line), file)) {
        if (row >= 8) {
            for (int i = 0; i < strlen(line); i++) {
                if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
                    if ((line[i] >= 'a' && line[i] <= 'm') || (line[i] >= 'A' && line[i] <= 'M')) {
                        line[i] += 13;
                    } else {
                        line[i] -= 13;
                    }
                }
            }
            fseek(file, -strlen(line), SEEK_CUR);
            fputs(line, file);
        }
        row++;
    }

        fclose(file);
}
```
 Fungsi ini untuk mendekripsi file extensions.csv yang telah diunduh. Fungsi ini nantinya akan membuka file, membaca isinya, mendekripsinya menggunakan metode Caesar cipher sederhana dengan pergeseran 13 karakter, dan menimpa file asli dengan konten yang telah didekripsi. Fungsi ini dipanggil setelah berhasil mengunduh file tersebut.

 ```
 void readExtensionsFromFile(const char* filename, char* extensions[], size_t maxExtensions) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        perror("File open error");
        exit(1);
    }

    char line[458];
    size_t count = 0;

    while (fgets(line, sizeof(line), file) && count < maxExtensions) {
        size_t len = strlen(line);
        if (len > 0 && line[len - 1] == '\n') {
            line[len - 1] = '\0';
        }

        extensions[count] = strdup(line);
        count++;

    }

    fclose(file);
}
 ```

Fungsi ini untuk membaca pola ekstensi dari file extensions.csv ke dalam sebuah array. Fungsi ini menerima nama file, sebuah array pola ekstensi (extensions), dan jumlah maksimum ekstensi (maxExtensions) sebagai parameter. Fungsi ini membuka file tersebut, membaca setiap baris (pola ekstensi) ke dalam array, dan menghapus karakter baris baru di akhir setiap baris. Fungsi ini juga menghitung jumlah ekstensi yang dibaca. Fungsi ini digunakan untuk memuat pola ekstensi yang akan digunakan dalam pemeriksaan selanjutnya.

```
void checkAndLogInfectedFiles() {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir("sisop_infected")) == NULL) {
        perror("opendir");
        exit(1);
    }

    char* extensions[458];
    size_t numExtensions = sizeof(extensions) / sizeof(extensions[0]);

    readExtensionsFromFile("extensions.csv", extensions, numExtensions);

    while ((entry = readdir(dir)) != NULL) {
        struct stat st;
        char filePath[512];
        snprintf(filePath, sizeof(filePath), "sisop_infected/%s", entry->d_name);

        if (lstat(filePath, &st) == -1) {
            perror("lstat");
            continue;
        }


        if (S_ISREG(st.st_mode)) {
            const char *ext = strrchr(entry->d_name, '.');
            if (ext) {
                ext++;  
                for (size_t i = 0; i < numExtensions; i++) {
                    if (extensions[i] != NULL) {
                        size_t extLen = strlen(extensions[i]);

                        size_t fileExtLen = strlen(ext);

                        if (extLen == fileExtLen && strncasecmp(extensions[i], ext, extLen) == 0) {


                            time_t now;
                            struct tm *tm_info;
                            char timestamp[20];
                            time(&now);
                            tm_info = localtime(&now);
                            strftime(timestamp, 20, "%d-%m-%y:%H-%M-%S", tm_info);

                            FILE *logFile = fopen("virus.log", "a");
                            if (logFile == NULL) {
                                perror("File open error");
                                exit(1);
                            }
                            fprintf(logFile, "[%s][%s] - %s - Moved to quarantine\n", "sisopUser", timestamp, entry->d_name);
                            fclose(logFile);

                            char quarantinePath[512];
                            snprintf(quarantinePath, sizeof(quarantinePath), "quarantine/%s", entry->d_name);
                            if (rename(filePath, quarantinePath) == -1) {
                                perror("rename");
                                exit(1);
                            }
                        }
                    }
                }


            }
        }
    }

    for (size_t i = 0; i < numExtensions; i++) {
        free(extensions[i]);
    }

    closedir(dir);
}

```

Fungsi ini merupakan inti dari program antivirus. Fungsi ini memeriksa semua file dalam direktori "sisop_infected" dan mencatat file yang terinfeksi ke dalam file "virus.log". Fungsi ini menggunakan array pola ekstensi yang dimuat dari extensions.csv untuk membandingkan dan mengidentifikasi file yang terinfeksi. Jika sebuah file cocok dengan salah satu pola ekstensi, maka file tersebut akan dicatat, dan program akan mencoba memindahkannya ke direktori "karantina". Informasi yang dicatat meliputi nama pengguna, cap waktu, nama file, dan tindakan yang diambil.

```

int main() {
    pid_t child_pid;
    int status;

    if ((child_pid = fork()) == 0) {
        downloadExtensionsCSV();
        exit(0);
    } else if (child_pid < 0) {
        perror("fork");
        exit(1);
    } else {
        waitpid(child_pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            decryptExtensionsCSV();
            checkAndLogInfectedFiles();
            printf("Program selesai.\n");
        } else {
            printf("Gagal mengunduh 'extensions.csv'.\n");
        }
    }

    return 0;
}
```

 Fungsi ini merupakan titik masuk utama dari program. Fungsi ini memulai proses dengan melakukan fork ke dalam proses anak untuk mengunduh file extensions.csv. Setelah unduhan, program memeriksa status keluaran dari proses. Jika unduhan berhasil, program akan melanjutkan dengan mendekripsi file dan melakukan pemeriksaan terhadap file yang terinfeksi. Jika unduhan gagal, program akan mencetak pesan kesalahan. Setelah pemeriksaan awal, program dapat dikonfigurasi untuk memasuki loop (saat ini dijeda) untuk secara berkala memeriksa dan mencatat file yang terinfeksi. Program diakhiri dengan pesan yang sesuai.
